mod threadpool;

use rand::prelude::*;
use std::sync::{Arc, Mutex};
use threadpool::ThreadPool;

fn main() -> Result<(), rand::Error> {
    let n_workers = num_cpus::get();
    let n_jobs = 1000000;
    let mut pool = ThreadPool::new(n_workers);
    let max = Arc::new(Mutex::new(0));

    for _ in 0..n_jobs {
        let data = max.clone();

        pool.execute(move |rng| {
            let mut local_max = 0;
            for _ in 0..1000 {
                let mut total = 0;
                for _ in 0..262 {
                    if rng.gen_bool(20.0/423.0) {
                        total += 1;
                    }
                }
                if total > local_max {
                    local_max = total;
                }
            }

            let mut current_max = data.lock().unwrap();
            if local_max > *current_max {
                *current_max = local_max;
                println!("new max: {}", local_max);
            }
        });
    }

    drop(pool);
    let result = max.lock().unwrap();
    println!("{}", result);
    Ok(())
}
