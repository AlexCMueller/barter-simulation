# barter-simulation
I made this to emulate piglin barters with the attempt of replicating Dream's luck,
with regards to only ender pearls.

This is a multithreaded simulation, so it should run much faster than other simulations
out there--on my machine it does over 100 billion RNG calls per minute. The tradeoff is
that this will eat up a lot of CPU power.

The secret sauce here is a custom threadpool that gives each worker thread its own source
of RNG, so you don't need to re-seed RNG every new job or share RNG between threads
causing excessive locking.
# Usage
`git clone` the repository, then run with `cargo run --release`. Rust runs much faster
in release mode, so it is important to use this flag.